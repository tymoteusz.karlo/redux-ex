import React from "react";
import "./App.css";
import AbilitiesContainer from "./app/abilities/components/AbilitiesContainer.js";
import AttributesContainer from "./app/attributes/components/AttributesContainer.js";
import CharacterInfoContainer from "./app/characterInfo/components/CharacterInfoContainer";

function App() {
  return (
    <div className="app">
      <div className="grid">
      <div className="sideBar">
        <header>CHARACTER CREATION RULES: </header>
        <li> Select Your Caste </li>
        <li> Select Supernal Ability </li>
        <li> Select Primary, Secondary and Tertiary Attributes </li>
        <li> Assign attribute points </li>
        <li> Select 4 caste abilities from caste abilities list </li>
        <li> Select additional 5 favoured abilities</li>
        <li> Assign ability points</li>
      </div>
      <div className="mainSection">
        <CharacterInfoContainer className="character-info-container" />
        <AttributesContainer />
        <AbilitiesContainer />
      </div>
      </div>
    </div>
  );
}

export default App;
