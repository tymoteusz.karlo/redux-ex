import * as types from '../constants/ActionTypes';
import AbilityArray from '../constants/ListOfAbilities';
import { Caste, Favoured, Supernal, None } from '../constants/ListOfAbilities';

const INITIAL_STATE = {
    abilities: AbilityArray,
    casteAbilities: 0,
    favouredAbilities: 0,
    pointsSpent: 0,
    pointsToSpend: 28
}

const abilitiesReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case types.RESET_ABILITIES:
            return {
                ...state,
                abilities: AbilityArray
            }

        case types.SET_SUPERNAL_ABILITY:
            {
                const abilities = state.abilities
                Object.keys(abilities).forEach(v => abilities[v].type === Supernal ? abilities[v].type = None : null)
                const ability = state.abilities.filter(obj => {
                    return obj.name === action.payload.abilityId
                })
                const key = abilities.findIndex(i => i.name === action.payload.abilityId)
                ability[0].type = Supernal
                abilities[key] = ability[0]
                let casteAbilitiesCount = 0, favouredAbilitiesCount = 0, i
                for (i = 0; i < abilities.length; ++i) {
                    if (abilities[i].type === Caste || abilities[i].type === Supernal) { ++casteAbilitiesCount }
                    else if (abilities[i].type === Favoured) { ++favouredAbilitiesCount }
                }
                return {
                    ...state,
                    abilities: abilities,
                    casteAbilities: casteAbilitiesCount,
                    favouredAbilities: favouredAbilitiesCount
                }
            }

        case types.RESET_SUPERNAL_ABILITY:
            {
                const abilities = state.abilities
                Object.keys(abilities).forEach(v => (abilities[v].type = None))
                return {
                    ...state,
                    abilities: abilities
                }
            }

        case types.UPDATE_CASTE_ABILITIES:
            {
                const casteAbilitiesList = action.payload.casteAbilitiesList
                const abilities = state.abilities
                const ability = Object.assign({}, state.abilities[action.payload.abilityId])
                if (ability.type !== Supernal) {
                    switch (ability.type) {
                        case Caste:
                            ability.type = None
                            break
                        case Favoured:
                            ability.type = None
                            break
                        case None:
                            if ((state.casteAbilities === 5 && state.favouredAbilities < 5) || (state.favouredAbilities < 5 && casteAbilitiesList.indexOf(ability.name) === -1)) {
                                ability.type = Favoured
                            }
                            else if ((state.casteAbilities === 5 && state.favouredAbilities === 5) || (state.favouredAbilities === 5 && casteAbilitiesList.indexOf(ability.name) === -1)) {
                                ability.type = None
                            }
                            else {
                                ability.type = Caste
                            }
                            break
                    }
                }
                abilities[action.payload.abilityId] = ability
                let casteAbilitiesCount = 0, favouredAbilitiesCount = 0, i
                for (i = 0; i < abilities.length; ++i) {
                    if (abilities[i].type === Caste || abilities[i].type === Supernal) { ++casteAbilitiesCount }
                    else if (abilities[i].type === Favoured) { ++favouredAbilitiesCount }
                }
                return {
                    ...state,
                    abilities: abilities,
                    casteAbilities: casteAbilitiesCount,
                    favouredAbilities: favouredAbilitiesCount
                }
            }

        case types.SET_VALUE_ABILITIES:
            {
                const pointsToSpend = state.pointsToSpend
                const pointsSpent = state.pointsSpent
                const pointsLeft = pointsToSpend - pointsSpent
                let ability = Object.assign({}, state.abilities[action.payload.abilityId])
                const prevAbilityValue = ability.value
                if (ability.value === action.payload.value) {
                    ability.value = 0

                    let abilities = state.abilities
                    abilities[action.payload.abilityId] = ability

                    let pointsSpent = 0, i
                    for (i = 0; i < abilities.length; ++i) {
                        pointsSpent += abilities[i].value
                    }
                    return {
                        ...state,
                        abilities: abilities,
                        pointsSpent: pointsSpent
                    }

                } else {
                    ability.value = action.payload.value

                    let abilities = state.abilities
                    abilities[action.payload.abilityId] = ability
                    let pointsSpent = 0, i
                    for (i = 0; i < abilities.length; ++i) {
                        pointsSpent += abilities[i].value
                    }

                    if (pointsToSpend < pointsSpent) {
                        const newValue = pointsLeft === 0 ? prevAbilityValue : prevAbilityValue + pointsLeft
                        ability.value = newValue
                        let pointsSpent = 0, i
                        for (i = 0; i < abilities.length; ++i) {
                            pointsSpent += abilities[i].value
                        }
                        return {
                            ...state,
                            abilities: abilities,
                            pointsSpent: pointsSpent
                        }
                    }
                    else {
                        return {
                            ...state,
                            abilities: abilities,
                            pointsSpent: pointsSpent
                        }
                    }
                }
            }

        default:
            return state
    }
}

export default abilitiesReducer