import * as types from '../constants/ActionTypes';
import { ListOfDawnAbilities, ListOfZenithAbilities, ListOfTwilightAbilities, ListOfNightAbilities, ListOfEclipseAbilities } from '../../app/constants/ListOfAbilities'

const INITIAL_STATE = {
    caste: null,
    name: null,
    playerName: null,
    concept: null,
    anima: null,
    supernalAbility: null,
    casteAbilitiesList: null
}

const characterInfoReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case types.SELECT_CASTE:
            {
                const caste = action.payload.caste
                let casteAbilitiesList
                switch (caste) {
                    case null:
                        casteAbilitiesList = []
                        break
                    case 'Dawn':
                        casteAbilitiesList = ListOfDawnAbilities
                        break
                    case 'Zenith':
                        casteAbilitiesList = ListOfZenithAbilities
                        break
                    case 'Twilight':
                        casteAbilitiesList = ListOfTwilightAbilities
                        break
                    case 'Night':
                        casteAbilitiesList = ListOfNightAbilities
                        break
                    case 'Eclipse':
                        casteAbilitiesList = ListOfEclipseAbilities
                        break
                }

                return {
                    ...state,
                    caste: caste,
                    casteAbilitiesList: casteAbilitiesList
                }
            }
            
        case types.CHARACTER_INFO_UPDATE:
            {
                console.log('action.payload', action.payload)
                const value = action.payload.value
                switch (action.payload.field) {
                    case 'name':
                        return {
                            ...state,
                            name: value
                        }

                    case 'playerName':
                        return {
                            ...state,
                            playerName: value
                        }
                    case 'concept':
                        return {
                            ...state,
                            concept: value
                        }
                    case 'anima':
                        return {
                            ...state,
                            anima: value
                        }
                    case 'supernalAbility':
                        return {
                            ...state,
                            supernalAbility: value
                        }
                    default:
                        return {
                            state
                        }
                }
            }
        default:
            return state
    }
}

export default characterInfoReducer