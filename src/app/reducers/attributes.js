import * as types from '../constants/ActionTypes';
import { Physical, Social, Mental } from '../constants/ListOfAttributes';

const PrimaryAttPool = 8
const SecondaryAttPool = 6
const TertiaryAttPool = 4
const Primary = {name: 'Primary', value: PrimaryAttPool}
const Secondary = {name: 'Secondary', value: SecondaryAttPool}
const Tertiary = {name: 'Tertiary', value: TertiaryAttPool}

const INITIAL_STATE = {
    Physical,
    Social,
    Mental,
    pointsSpent:
        [0, 0, 0],
    pointsToSpend:
        [PrimaryAttPool, SecondaryAttPool, TertiaryAttPool],
    importance:
        [Primary, Secondary, Tertiary]
}

const attributesReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case types.RESET_ATTRIBUTES:
            {
                let i, j, k
                for (i = 0; i < Physical.length; ++i) {
                    Physical[i].value = 1
                }

                for (j = 0; j < Social.length; ++j) {
                    Social[j].value = 1
                }
                for (k = 0; k < Mental.length; ++k) {
                    Mental[k].value = 1
                }

                console.log('RESET_ATTRIBUTES',
                    Physical,
                    Social,
                    Mental)
                return {
                    ...state,
                    Physical,
                    Social,
                    Mental,
                    pointsSpent:
                        [0, 0, 0]
                }
            }

        case types.SET_IMPORTANCE:
            {
                console.log('SET_IMPORTANCE', action)
                const newImportance = [...state.importance]
                newImportance.forEach(function(item, i) { if (item.name === action.payload.importance) newImportance[i] = {name: '', value: 0}});

                console.log('items', action.payload.importance, newImportance, state.importance)

                switch (action.payload.groupName) {
                    case 'Physical':
                        console.log('SWITCH Physical')
                        switch (action.payload.importance) {
                            case 'Primary': {
                                // newPointsPool[0] = PrimaryAttPool
                                newImportance[0] = Primary
                                return {
                                    ...state,
                                    // pointsToSpend: newPointsPool,
                                    importance: newImportance
                                }
                            }
                            case 'Secondary': {
                                // newPointsPool[0] = SecondaryAttPool
                                newImportance[0] = Secondary
                                return {
                                    ...state,
                                    // pointsToSpend: newPointsPool,
                                    importance: newImportance
                                }
                            }
                            case 'Tertiary': {
                                // newPointsPool[0] = TertiaryAttPool
                                newImportance[0] = Tertiary
                                return {
                                    ...state,
                                    // pointsToSpend: newPointsPool,
                                    importance: newImportance
                                }
                            }
                            default: {
                                return {
                                    ...state,
                                    // pointsToSpend: newPointsPool,
                                    importance: newImportance
                                }
                            }
                        }

                    case 'Social':
                        console.log('SWITCH Social')
                        switch (action.payload.importance) {
                            case 'Primary': {
                                // newPointsPool[1] = PrimaryAttPool
                                newImportance[1] = Primary
                                return {
                                    ...state,
                                    // pointsToSpend: newPointsPool,
                                    importance: newImportance
                                }
                            }
                            case 'Secondary': {
                                // newPointsPool[1] = SecondaryAttPool
                                newImportance[1] = Secondary
                                return {
                                    ...state,
                                    // pointsToSpend: newPointsPool,
                                    importance: newImportance
                                }
                            }
                            case 'Tertiary': {
                                // newPointsPool[1] = TertiaryAttPool
                                newImportance[1] = Tertiary
                                return {
                                    ...state,
                                    // pointsToSpend: newPointsPool,
                                    importance: newImportance
                                }
                            }
                            default: {
                                return {
                                    ...state,
                                    // pointsToSpend: newPointsPool,
                                    importance: newImportance
                                }
                            }
                        }
                    case 'Mental':
                        console.log('SWITCH Mental')
                        switch (action.payload.importance) {
                            case 'Primary': {
                                // newPointsPool[2] = PrimaryAttPool
                                newImportance[2] = Primary
                                return {
                                    ...state,
                                    // pointsToSpend: newPointsPool,
                                    importance: newImportance
                                }
                            }
                            case 'Secondary': {
                                // newPointsPool[2] = SecondaryAttPool
                                newImportance[2] = Secondary
                                return {
                                    ...state,
                                    // pointsToSpend: newPointsPool,
                                    importance: newImportance
                                }
                            }
                            case 'Tertiary': {
                                // newPointsPool[2] = TertiaryAttPool
                                newImportance[2] = Tertiary
                                return {
                                    ...state,
                                    // pointsToSpend: newPointsPool,
                                    importance: newImportance
                                }
                            }
                            default: {
                                return {
                                    ...state,
                                    // pointsToSpend: newPointsPool,
                                    importance: newImportance
                                }
                            }
                        }
                    default:
                    case 'Primary': {
                        newImportance[0] = PrimaryAttPool
                        return {
                            ...state,
                            importance: newImportance
                        }
                    }
                    case 'Secondary': {
                        newImportance[0] = SecondaryAttPool
                        return {
                            ...state,
                            importance: newImportance
                        }
                    }
                    case 'Tertiary': {
                        newImportance[0] = TertiaryAttPool
                        return {
                            ...state,
                            importance: newImportance
                        }
                    }
                }
            }


        case types.SET_VALUE_ATTRIBUTES:
            {
                let group = [action.payload.group]
                let pointsToSpendLoc
                let pointsSpentLoc
                let pointsIndex
                console.log('GROUP', group)
                switch (action.payload.group) {
                    case 'Physical':
                        pointsToSpendLoc = action.payload.importance.value
                        pointsSpentLoc = state.pointsSpent[0]
                        pointsIndex = 0
                        console.log('CASE 1', pointsToSpendLoc, pointsSpentLoc, pointsIndex)
                        break;
                    case 'Social':
                        pointsToSpendLoc = action.payload.importance.value
                        pointsSpentLoc = state.pointsSpent[1]
                        pointsIndex = 1
                        console.log('CASE 2', pointsToSpendLoc, pointsSpentLoc, pointsIndex)
                        break;
                    case 'Mental':
                        pointsToSpendLoc = action.payload.importance.value
                        pointsSpentLoc = state.pointsSpent[2]
                        pointsIndex = 2
                        console.log('CASE 3', pointsToSpendLoc, pointsSpentLoc, pointsIndex)
                        break;
                    default:
                        pointsToSpendLoc = action.payload.importance.value
                        pointsSpentLoc = state.pointsSpent[0]
                        pointsIndex = 0
                }
                console.log('SWITCH END', pointsToSpendLoc, pointsSpentLoc, pointsIndex, group)

                const pointsLeft = pointsToSpendLoc - pointsSpentLoc
                const attribute = Object.assign({}, state[group][action.payload.attributeId])
                const prevAttributeValue = attribute.value
                if (attribute.value === action.payload.value) {
                    attribute.value = 1
                    let attributes = state[group]
                    console.log('attributes', attributes)
                    attributes[action.payload.attributeId] = attribute

                    let pointsSpentLoc = 0, i
                    for (i = 0; i < attributes.length; ++i) {
                        pointsSpentLoc += attributes[i].value - 1
                    }
                    const newPoints = [...state.pointsSpent]
                    newPoints[pointsIndex] = pointsSpentLoc
                    console.log('state.pointsSpent, newPoints, pointsToSpendLoc, pointsSpentLoc ===', state.pointsSpent, newPoints, pointsSpentLoc)
                    return {
                        ...state,
                        Physical,
                        Social,
                        Mental,
                        pointsSpent: newPoints
                    }

                } else {
                    attribute.value = action.payload.value
                    let attributes = state[group]
                    attributes[action.payload.attributeId] = attribute
                    let pointsSpentLoc = 0, i
                    for (i = 0; i < attributes.length; ++i) {
                        pointsSpentLoc += attributes[i].value - 1
                    }
                    const newPoints = [...state.pointsSpent]
                    newPoints[pointsIndex] = pointsSpentLoc
                    console.log('state.pointsSpent, newPoints, pointsToSpendLoc, pointsSpentLoc', state.pointsSpent, newPoints, pointsToSpendLoc, pointsSpentLoc)
                    console.log('pointsToSpendLoc < pointsSpentLoc', pointsToSpendLoc < pointsSpentLoc, pointsToSpendLoc, pointsSpentLoc)
                    if (pointsToSpendLoc < pointsSpentLoc) {
                        console.log('prevAttributeValue, pointsLeft', prevAttributeValue, pointsLeft)
                        const newValue = pointsLeft === 0 ? prevAttributeValue : prevAttributeValue + pointsLeft
                        attribute.value = newValue
                        let pointsSpentLoc = 0, i
                        for (i = 0; i < attributes.length; ++i) {
                            pointsSpentLoc += attributes[i].value - 1
                        }
                        const newPoints = [...state.pointsSpent]
                        newPoints[pointsIndex] = pointsSpentLoc
                        return {
                            ...state,
                            Physical,
                            Social,
                            Mental,
                            pointsSpent: newPoints
                        }
                    }
                    else {
                        return {
                            ...state,
                            Physical,
                            Social,
                            Mental,
                            pointsSpent: newPoints
                        }
                    }
                }
            }


        default:
            return state
    }
}

export default attributesReducer