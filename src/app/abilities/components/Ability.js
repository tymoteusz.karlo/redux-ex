import React, { Component } from 'react';
import PropTypes from "prop-types";
import Dot from '../../visuals/Dot';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { setValue, updateCasteAbilities } from '../../actions/abilities';
import Checkbox from '@material-ui/core/Checkbox';
import Square from '../../../static/images/Square.png';
import SquareBlack from '../../../static/images/SquareBlack.png';
import SquarePurple from '../../../static/images/SquarePurple.png';
import SquareGrey from '../../../static/images/SquareGrey.png';
import { Supernal, Caste, Favoured } from '../../constants/ListOfAbilities'

const Dots = styled.div`
    display: flex;
`
class Ability extends Component {

    static propTypes = {
        value: PropTypes.number,
        id: PropTypes.number,
        key: PropTypes.number,
        caste: PropTypes.bool,
        favoured: PropTypes.bool,
        supernal: PropTypes.bool,
        name: PropTypes.string,
        type: PropTypes.string
    };

    handleSetValue = (abilityValue) => {
        const { id, setValue } = this.props
        setValue(id, abilityValue)
    }

    handleChange = () => {
        const { id, updateCasteAbilities } = this.props
        this.props.supernalAbility === null ? alert('Select Supernal Ability') : updateCasteAbilities(id)
    }

    render() {
        const buttons = [1, 2, 3, 4, 5]
        return (
            <div key={this.props.name} className="ability-container">
                <Checkbox
                    checked={this.props.type === Supernal || this.props.type === Caste || this.props.type === Favoured ? true : false}
                    icon={<img width='15px' src={Square} />}
                    checkedIcon={this.props.type === Supernal ? <img width='15px' src={SquarePurple} /> : (this.props.type === Favoured ? <img width='15px' src={SquareGrey} /> : <img width='15px' src={SquareBlack} />)}
                    onChange={this.handleChange}
                />
                <div className={this.props.name}>{this.props.name}</div>
                <Dots>
                    {buttons.map(item => {
                        return <Dot
                            key={item} value={item} setValue={this.handleSetValue} pressed={this.props.value < item ? false : true} onClick={(e) => this.handleChange(e)} type={'ability'} />
                    })}
                </Dots>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setValue: (id, value) => {
            dispatch(setValue(id, value))

        },
        updateCasteAbilities: (id) => {
            dispatch(updateCasteAbilities(id))
        }
    }

}

export default connect(null, mapDispatchToProps)(Ability)