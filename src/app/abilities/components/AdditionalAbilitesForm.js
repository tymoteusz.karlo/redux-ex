import React, { useState, useEffect, useRef } from 'react';
import _uniqueId from 'lodash/uniqueId';


const AdditionalAbilitiesForm = (props) => {
    const [id] = useState(_uniqueId('prefix-'));


    const [input, setInput] = useState(props.edit ? props.edit.value : '');

    const inputRef = useRef(null);

    useEffect(() => {
        inputRef.current.focus();
    });

    const handleChange = e => {
        setInput(e.target.value);
    };

    const handleSubmit = e => {
        e.preventDefault();

        props.onSubmit({
            id: id,
            text: input
        });
        setInput('');
    };

    return (
        <form onSubmit={handleSubmit} className='add-abs-form-form' >
            {
                props.edit ? (
                    <div>
                        <input
                            placeholder='Update your item'
                            value={input}
                            onChange={handleChange}
                            name='text'
                            ref={inputRef}
                            className='add-abs-form-input edit'
                        />
                        <button onClick={handleSubmit} className='add-abs-form-button edit'>
                            Update
          </button>
                    </div>
                ) : (
                        <div>
                            <input
                                placeholder='Add an additional ability'
                                value={input}
                                onChange={handleChange}
                                name='text'
                                className='add-abs-form-input'
                                ref={inputRef}
                            />
                            <button onClick={handleSubmit} className='add-abs-form-button'>
                                Add an ability
          </button>
                        </div>
                    )
            }
        </form>
    );
}

export default AdditionalAbilitiesForm;