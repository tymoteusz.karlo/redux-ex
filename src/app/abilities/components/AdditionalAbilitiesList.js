import React, { useState } from 'react';
import AdditionalAbilitiesForm from './AdditionalAbilitesForm';
import AdditionalAbilities from './AdditionalAbilities';

function AdditionalAbilitiesList() {
  const [addAbilities, setAbilities] = useState([]);

  const addAbility = ability => {
    if (!ability.text || /^\s*$/.test(ability.text)) {
      return;
    }

    const newAbilities = [ability, ...addAbilities];

    setAbilities(newAbilities);
    console.log(...addAbilities);
  };

  const updateAbility = (abilityId, newValue) => {
    if (!newValue.text || /^\s*$/.test(newValue.text)) {
      return;
    }

    setAbilities(prev => prev.map(item => (item.id === abilityId ? newValue : item)));
  };

  const removeAbility = id => {
    const removedArr = [...addAbilities].filter(ability => ability.id !== id);

    setAbilities(removedArr);
  };

  const completeAbility = id => {
    let updatedAbilities = addAbilities.map(ability => {
      if (ability.id === id) {
        ability.isComplete = !ability.isComplete;
      }
      return ability;
    });
    setAbilities(updatedAbilities);
  };

  return (
    <>
      <AdditionalAbilitiesForm onSubmit={addAbility} />
      <AdditionalAbilities
        addAbilities={addAbilities}
        completeAbility={completeAbility}
        removeAbility={removeAbility}
        updateAbility={updateAbility}
      />
    </>
  );
}

export default AdditionalAbilitiesList;