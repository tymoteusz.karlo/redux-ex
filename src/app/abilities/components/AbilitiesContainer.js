import React, { Component } from "react";
import Ability from "./Ability";
import AdditionalAbilitiesList from "./AdditionalAbilitiesList";
import { connect } from "react-redux";

export class AbilitiesContainer extends Component {
  render() {
    const {
      abilities,
      pointsSpent,
      pointsToSpend,
      casteAbilities,
      favouredAbilities,
    } = this.props.abilities;
    const { supernalAbility, casteAbilitiesList } = this.props.characterInfo;
    return (
      <div className="caste-abilities-list">
        <div>ABILITIES</div>

        {casteAbilitiesList ? (
          <div className="caste-abilities-list">
            Caste abilities list:
            <ul id="stats">
              {casteAbilitiesList ? (
                casteAbilitiesList.map((ability) => (
                  <li>
                    <span>{ability}</span>
                  </li>
                ))
              ) : (
                <></>
              )}
            </ul>
          </div>
        ) : (
          <></>
        )}
        <div className="abilities-container">
          <span>Caste abilities selected: {casteAbilities} </span>
          <span>Favoured abilities selected: {favouredAbilities}</span>
          <span>Points to spend: {pointsToSpend - pointsSpent} </span>
          {abilities !== undefined
            ? abilities.map((ability, i) => (
                <Ability
                  key={ability.name}
                  id={i}
                  name={ability.name}
                  value={ability.value}
                  type={ability.type}
                  pointsSpent={pointsSpent}
                  supernalAbility={supernalAbility}
                />
              ))
            : null}
        </div>
        {/* <AdditionalAbilitiesList /> */}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    abilities: state.abilities,
    characterInfo: state.characterInfo,
  };
};

export default connect(mapStateToProps, {})(AbilitiesContainer);
