import React, { useState } from "react";
import AdditionalAbilitesForm from "./AdditionalAbilitesForm";
import Ability from "./Ability";
import { IconButton } from "@material-ui/core";
import ClearSharpIcon from "@mui/icons-material/ClearSharp";
import EditSharpIcon from "@mui/icons-material/EditSharp";

const AdditionalAbilities = ({
  addAbilities,
  completeAbility,
  removeAbility,
  updateAbility,
}) => {
  const [edit, setEdit] = useState({
    id: null,
    value: "",
  });

  const submitUpdate = (value) => {
    updateAbility(edit.id, value);
    setEdit({
      id: null,
      value: "",
    });
  };

  if (edit.id) {
    return <AdditionalAbilitesForm edit={edit} onSubmit={submitUpdate} />;
  }

  return addAbilities.map((ability, index) => (
    <div
      className={ability.isComplete ? "ability-row complete" : "ability-row"}
      key={index}
    >
      <div key={ability.id} onClick={() => completeAbility(ability.id)}>
        <Ability
          key={ability.text}
          id={ability.id}
          name={ability.text}
          value={ability.value}
          type={ability.type}
        ></Ability>
        <IconButton onClick={() => removeAbility(ability.id)}>
          <ClearSharpIcon fontSize="small" />
        </IconButton>
        <IconButton
          onClick={() => setEdit({ id: ability.id, value: ability.text })}
        >
          <EditSharpIcon fontSize="small" />
        </IconButton>
      </div>
      <div className="icons"></div>
    </div>
  ));
};

export default AdditionalAbilities;
