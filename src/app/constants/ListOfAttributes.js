export const ListOfAttributes = [
    'Strength',
    'Dexterity',
    'Stamina',
    'Charisma',
    'Manipulation',
    'Appearance',
    'Perception',
    'Intelligence',
    'Wits'
]

const PhysicalAttributes = ListOfAttributes.slice(0,3).reduce((o, key) => ({
    ...o, [key]: {
        name: key,
        value: 1,
        attributes: 'primary'
    }
}), {})
const SocialAttributes = ListOfAttributes.slice(3,6).reduce((o, key) => ({
    ...o, [key]: {
        name: key,
        value: 1,
        attributes: 'secondary'
    }
}), {})
const MentalAttributes = ListOfAttributes.slice(6,9).reduce((o, key) => ({
    ...o, [key]: {
        name: key,
        value: 1,
        attributes: 'tertiary'
    }
}), {})

export const Physical = Object.values(PhysicalAttributes);
export const Social = Object.values(SocialAttributes);
export const Mental = Object.values(MentalAttributes);

export const Attributes = {Physical, Social, Mental};

