export const Supernal = 'supernal'
export const Caste = 'caste'
export const Favoured = 'favoured'
export const None = 'none'

export const ListOfAbilities = [
    'Archery',
    'Athletics',
    'Awareness',
    'Brawl',
    'Bureaucracy',
    'Craft',
    'Dodge',
    'Integrity',
    'Investigation',
    'Larceny',
    'Linguistics',
    'Lore',
    'Martial Arts',
    'Medicine',
    'Melee',
    'Occult',
    'Performance',
    'Presence',
    'Resistance',
    'Ride',
    'Sail',
    'Socialize',
    'Stealth',
    'Survival',
    'Thrown',
    'War']

export const ListOfDawnAbilities = [
    'Archery',
    'Awareness',
    'Brawl',
    'Dodge',
    'Melee',
    'Resistance',
    'Thrown',
    'War'
]

export const ListOfZenithAbilities = [
    'Athletics',
    'Integrity',
    'Performance',
    'Lore',
    'Presence',
    'Resistance',
    'Survival',
    'War'
]

export const ListOfTwilightAbilities = [
    'Bureaucracy',
    'Craft',
    'Integrity',
    'Investigation',
    'Linguistics',
    'Lore',
    'Medicine',
    'Occult'
]

export const ListOfNightAbilities = [
    'Athletics',
    'Awareness',
    'Dodge',
    'Investigation',
    'Larceny',
    'Ride',
    'Stealth',
    'Socialize'
]

export const ListOfEclipseAbilities = [
    'Bureaucracy',
    'Larceny',
    'Linguistics',
    'Occult',
    'Presence',
    'Ride',
    'Sail',
    'Socialize'
]

const Abilities = ListOfAbilities.reduce((o, key) => ({
    ...o, [key]: {
        name: key,
        value: 0,
        specialisation: null,
        type: None
    }
}), {})


export const AbilityArray = Object.values(Abilities);

export default AbilityArray