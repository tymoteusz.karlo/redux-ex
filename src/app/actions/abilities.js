import * as types from '../constants/ActionTypes';
import store from '../../../src/store'

export const actionFailed = (actionType, errorMessage, object) => {
    let action = {
        type: actionType,
        status: 'error',
        isFetching: false
    }

    if (errorMessage) {
        action.errorMessage = errorMessage
    }
    if (object) {
        action = Object.assign(action, object)
    }
    return action
}

export const actionStart = (actionType, object) => {
    return Object.assign({
        type: actionType,
        isFetching: true
    }, object)
}

export const actionSuccess = (actionType, object) => {
    return Object.assign({
        type: actionType,
        status: 'success',
        isFetching: false
    }, object)
}

export const setValue = (abilityId, value) => {
    return (dispatch) => {
        dispatch(actionSuccess(types.SET_VALUE_ABILITIES, {
            payload: {
                abilityId, value
            }
        }))
    }
}

export const setSupernalAbility = (abilityId) => {
    return (dispatch) => {
        dispatch(actionSuccess(types.SET_SUPERNAL_ABILITY, {
            payload: {
                abilityId
            }
        }))
    }
}

export const resetSupernalAbility = () => {
    return (dispatch) => {
        dispatch(actionSuccess(types.RESET_SUPERNAL_ABILITY, {
            payload: {
            }
        }))
    }
}


export const updateCasteAbilities = (abilityId) => {
    const casteAbilitiesList = store.getState().characterInfo.casteAbilitiesList
    return (dispatch) => {
        dispatch(actionSuccess(types.UPDATE_CASTE_ABILITIES, {
            payload: {
                abilityId, casteAbilitiesList
            }
        }))
    }
}

export const resetValues = () => {
    return (dispatch) => {
        dispatch(actionSuccess(types.RESET_ABILITIES, {
            payload: {
            }
        }))
    }
}