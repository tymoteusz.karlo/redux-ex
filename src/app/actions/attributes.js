import * as types from '../constants/ActionTypes';


export const actionFailed = (actionType, errorMessage, object) => {
    let action = {
        type: actionType,
        status: 'error',
        isFetching: false
    }

    if (errorMessage) {
        action.errorMessage = errorMessage
    }
    if (object) {
        action = Object.assign(action, object)
    }
    return action
}

export const actionStart = (actionType, object) => {
    return Object.assign({
        type: actionType,
        isFetching: true
    }, object)
}

export const actionSuccess = (actionType, object) => {
    return Object.assign({
        type: actionType,
        status: 'success',
        isFetching: false
    }, object)
}

export const setValueAtt = (attributeId, value, group, importance) => {
    return (dispatch) => {
        dispatch(actionSuccess(types.SET_VALUE_ATTRIBUTES, {
            payload: {
                attributeId, value, group, importance
            }
        }))
    }
}

export const resetValuesAtt = () => {
    return (dispatch) => {
        dispatch(actionSuccess(types.RESET_ATTRIBUTES, {
        }))
    }
}

export const setImportance = (groupName, importance) => {
    return (dispatch) => {
        dispatch(actionSuccess(types.RESET_ATTRIBUTES, {
        }))
        dispatch(actionSuccess(types.SET_IMPORTANCE, {
            payload: {
                groupName, importance
            }
        }))
    }
}

