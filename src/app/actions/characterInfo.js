import * as types from '../constants/ActionTypes';


export const actionFailed = (actionType, errorMessage, object) => {
    let action = {
        type: actionType,
        status: 'error',
        isFetching: false
    }

    if (errorMessage) {
        action.errorMessage = errorMessage
    }
    if (object) {
        action = Object.assign(action, object)
    }
    return action
}

export const actionStart = (actionType, object) => {
    return Object.assign({
        type: actionType,
        isFetching: true
    }, object)
}

export const actionSuccess = (actionType, object) => {
    return Object.assign({
        type: actionType,
        status: 'success',
        isFetching: false
    }, object)
}

export const selectCaste = (caste) => {
    return (dispatch) => {
        dispatch(actionSuccess(types.SELECT_CASTE, {
            payload: {
                caste
            }
        }))
    }
}

export const characterInfoUpdate = (field, value) => {
    return (dispatch) => {
        dispatch(actionSuccess(types.CHARACTER_INFO_UPDATE, {
            payload: {
                field, value
            }
        }))
    }
}