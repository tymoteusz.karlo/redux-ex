import React, { PureComponent } from 'react';
import PropTypes from "prop-types";
import styled from 'styled-components';
import full from '../../static/images/full.png';
import empty from '../../static/images/empty.png';


const DotImage = styled.img`
display: inline-block;
width: 20px;
height: 20px;
vertical-align: top;
margin-top: 2px;
margin-right: 1px;
`

class Dot extends PureComponent {
    static propTypes = {
        setValue: PropTypes.func,
        setValueAtt: PropTypes.func,
        value: PropTypes.number,
        pressed: PropTypes.bool,
        type: PropTypes.string
    };

    handleOnClick = () => {
        const {value, type} = this.props
        if (type === 'ability') {this.props.setValue(value)}
        if (type === 'attribute') {this.props.setValueAtt(value)}
    }

    render() {
        var pic = this.props.pressed ? full : empty
        return (
            <DotImage
                src={pic}
                onClick={() => this.handleOnClick()}
            ></DotImage>
        )
    }
}


export default Dot