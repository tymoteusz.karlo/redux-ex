import React, { Component } from "react";
import SelectCaste from "././SelectCaste";

export class CasteSelection extends Component {
  render() {
    const { caste } = this.props;

    return (
      <div class="caste-select">
        <label>SELECT YOUR CASTE: </label>
        <div>
          <SelectCaste caste={caste.caste} />
        </div>
      </div>
    );
  }
}

export default CasteSelection;
