import { selectCaste, characterInfoUpdate } from '../../actions/characterInfo'
import { resetSupernalAbility } from '../../actions/abilities'
import { connect } from 'react-redux';
import React from 'react';

const handleSelectCaste = (selectCaste, resetSupernalAbility, characterInfoUpdate, caste) => {
    return (selectCaste(caste), resetSupernalAbility(), characterInfoUpdate('supernalAbility', null))
}

function SelectCaste({ selectCaste, resetSupernalAbility, characterInfoUpdate, caste }) {
    return (
        <div className="importance">
            <div className="importance-content">
                <button className={('Dawn' === caste) ? "selected" : ""} onClick={() => handleSelectCaste(selectCaste, resetSupernalAbility, characterInfoUpdate,'Dawn')}>{'Dawn'}</button>
                <button className={('Zenith' === caste) ? "selected" : ""} onClick={() => handleSelectCaste(selectCaste, resetSupernalAbility, characterInfoUpdate,'Zenith')}>{'Zenith'}</button>
                <button className={('Twilight' === caste) ? "selected" : ""} onClick={() => handleSelectCaste(selectCaste, resetSupernalAbility, characterInfoUpdate,'Twilight')}>{'Twilight'}</button>
                <button className={('Night' === caste) ? "selected" : ""} onClick={() => handleSelectCaste(selectCaste, resetSupernalAbility, characterInfoUpdate,'Night')}>{'Night'}</button>
                <button className={('Eclipse' === caste) ? "selected" : ""} onClick={() => handleSelectCaste(selectCaste, resetSupernalAbility, characterInfoUpdate,'Eclipse')}>{'Eclipse'}</button>
            </div>
        </div>
    )
}

const mapDispatchToProps = dispatch => ({
    selectCaste: (caste) => dispatch(selectCaste(caste)),
    characterInfoUpdate: (field, value) => dispatch(characterInfoUpdate(field, value)),
    resetSupernalAbility: (value) => dispatch(resetSupernalAbility(value))
});

export default connect(
    null,
    mapDispatchToProps
)(SelectCaste);

