
import { connect } from 'react-redux';
import React, { Component } from 'react';
import CasteSelection from './CasteSelection';
import CharacterInfo from './CharacterInfo';

export class CharacterInfoContainer extends Component {

    render() {
        return (
            <div className="characterInfo-container">
                <CasteSelection caste={this.props.characterInfo} />
                <CharacterInfo characterInfo={this.props.characterInfo} />
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        characterInfo: state.characterInfo
    }
}

export default connect(mapStateToProps, null)(CharacterInfoContainer)