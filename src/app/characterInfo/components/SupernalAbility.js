import React, { Component } from 'react';
import PropTypes from "prop-types";
import { ListItemText, ListItem } from "@material-ui/core";

export class SupernalAbility extends Component {

    static propTypes = {
        onSelect: PropTypes.func,
    };

    handleSelect = () => {
      const { id } = this.props;
        this.props.onSelect(id);
    };

    render() {
        return (<ListItem button className="list-item" onClick={this.handleSelect}>
          <ListItemText primary={this.props.id}></ListItemText>
        </ListItem>);
      }
    }
    