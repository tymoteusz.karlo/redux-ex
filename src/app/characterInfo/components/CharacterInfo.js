import React, { Component } from "react";
import { connect } from "react-redux";
import { characterInfoUpdate } from "../../actions/characterInfo";
import { setSupernalAbility } from "../../actions/abilities";
import { Input, TextField } from "@material-ui/core";
import { withStyles, createStyles } from "@material-ui/core/styles";
import { SupernalAbilityButton } from "../components/SupernalAbilityButton";

const styles = (theme) =>
  createStyles({
    root: {
      "& .MuiInputBase-input": {
        color: "white",
      },
      "& .MuiFormLabel-root": {
        color: "white",
      },
      "&.Mui-focused fieldset": {
        borderColor: "white",
        color: "white",
      },
      "& label.Mui-focused": {
        color: "white",
      },
      "& .MuiInput-underline:after": {
        borderBottomColor: "white",
        color: "white",
      },
      "& .MuiOutlinedInput-root": {
        "& fieldset": {
          borderColor: "white",
          color: "white",
        },
        "&:hover fieldset": {
          borderColor: "white",
          color: "white",
        },
        "&.Mui-focused fieldset": {
          borderColor: "white",
          color: "white",
        },
      },
    },
  });

export class CharacterInfo extends Component {
  handleChange = (field, value) => {
    this.props.characterInfoUpdate(field, value);
  };

  selectAbility = (value) => {
    this.props.characterInfoUpdate("supernalAbility", value);
    this.props.setSupernalAbility(value);
  };

  render() {
    console.log("this.props", this.props);
    const { classes } = this.props;
    return (
      <div className="characterInfo-section">
        <span /*div class="form-style-heading"*/>CHARACTER INFO</span>
        {/* {this.props.characterInfo.caste === null ? <div className='color-overlay'></div> : null} */}
        <div class="form-style">
          <div>
          <TextField
            className={classes.root}
            label="NAME"
            variant="outlined"
            size="small"
            onBlur={(e) => this.handleChange("name", e.target.value)}
          ></TextField>
          </div>
          <div>
          <TextField
            className={classes.root}
            label="PLAYER NAME"
            variant="outlined"
            size="small"
            onBlur={(e) => this.handleChange("playerName", e.target.value)}
          ></TextField>{" "}
          </div>
          <div>
          <TextField
            className={classes.root}
            label="CONCEPT"
            variant="outlined"
            size="small"
            onBlur={(e) => this.handleChange("concept", e.target.value)}
          ></TextField>{" "}
          </div>
          <div>
          <TextField
            className={classes.root}
            label="ANIMA"
            variant="outlined"
            size="small"
            onBlur={(e) => this.handleChange("anima", e.target.value)}
          ></TextField>
          </div>
          <span>SUPERNAL ABILITY: </span>
          <SupernalAbilityButton
            caste={this.props.characterInfo.caste}
            value={this.props.characterInfo.supernalAbility}
            onSelectAbility={this.selectAbility}
          />
          <div>{this.props.supernalAbility}</div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  characterInfoUpdate: (field, value) =>
    dispatch(characterInfoUpdate(field, value)),
  setSupernalAbility: (value) => dispatch(setSupernalAbility(value)),
});

export default withStyles(styles)(
  connect(null, mapDispatchToProps)(CharacterInfo)
);
