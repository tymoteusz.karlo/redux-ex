import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { Button, Popover, List } from "@material-ui/core";
import { ListOfDawnAbilities, ListOfZenithAbilities, ListOfTwilightAbilities, ListOfNightAbilities, ListOfEclipseAbilities } from '../../../app/constants/ListOfAbilities'
import { SupernalAbility } from './SupernalAbility'

export class SupernalAbilityButton extends PureComponent {

    static propTypes = {
        onSelectAbility: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {
            openSelect: false,
        };
    }

    handleOpenSelectForm = event => {
        // This prevents ghost click.
        event.preventDefault();
        this.setState({
            openSelect: true,
            anchorEl: event.currentTarget
        });
    };
    handleOpenSelect = () => {
        this.setState({
            openSelect: false
        });
    };

    handleAbilitySelect = (id) => {
        this.props.onSelectAbility(id);
        this.handleOpenSelect();
    };

    handleListOfAbilities = (caste) => {
        switch (caste) {
            case null:
                return []
            case 'Dawn':
                return ListOfDawnAbilities
            case 'Zenith':
                return ListOfZenithAbilities
            case 'Twilight':
                return ListOfTwilightAbilities
            case 'Night':
                return ListOfNightAbilities
            case 'Eclipse':
                return ListOfEclipseAbilities
        }
    }

    handleNoSASelected = () => {
        alert('Select Caste first')
    }

    render() {
        const { caste } = this.props
        const listOfAbilities = this.handleListOfAbilities(caste)
        return caste === null ? <span>SELECT YOUR CASTE</span> :
            <div>
                <Button className="button-supernal-ability"
                    onClick={this.handleOpenSelectForm}
                    onFocus={this.handleOpenSelectForm}
                    onBlur={document.activeElement.blur()}>
                    {this.props.value === null ? "Select supernal ability" : this.props.value}
                </Button>
                <Popover
                    open={this.state.openSelect}
                    anchorEl={this.state.anchorEl}
                    anchorOrigin={{ horizontal: "left", vertical: "bottom" }}
                    transformOrigin={{ horizontal: "left", vertical: "top" }}
                    onClose={this.handleOpenSelect}
                    className="container-select">
                    <List>
                        {listOfAbilities.map(item => {
                            return <SupernalAbility key={item} id={item} onSelect={this.handleAbilitySelect} />
                        })}
                    </List>
                </Popover>
            </div>;
    }
}


