import { resetValuesAtt } from '../../actions/attributes'
import { connect } from 'react-redux';
import React from 'react';

function Reset({ resetValuesAtt }) {
    return (
        <button onClick={resetValuesAtt}>{'RESET'}</button>
    )
}

const mapDispatchToProps = dispatch => ({
    resetValuesAtt: () => dispatch(resetValuesAtt())
});

export default connect(
    null,
    mapDispatchToProps
)(Reset);