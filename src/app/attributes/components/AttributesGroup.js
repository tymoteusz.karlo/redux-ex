import React, { Component } from "react";
import Attribute from "./Attribute";
import SetImportance from "././SetImportance";

export class AttributesGroup extends Component {
  render() {
    const { attributes, group, importance, pointsToSpend } = this.props;
    return (
      <div className="attributes-group">
        <div>
          <div className="section-name">{group}</div>
          <div className="attributes-importance">
            <SetImportance group={group} importance={importance} />
            <div className="points">
              {importance.value - this.props.pointsSpent}
            </div>
          </div>
        </div>
        <div className="attributes-points">
          {attributes !== undefined
            ? attributes.map((attribute, i) => (
                <Attribute
                  key={attribute.name}
                  id={i}
                  name={attribute.name}
                  value={attribute.value}
                  importance={importance}
                  group={group}
                />
              ))
            : null}
        </div>
      </div>
    );
  }
}

export default AttributesGroup;
