import { setImportance } from '../../actions/attributes'
import { connect } from 'react-redux';
import React from 'react';

function SetImportance({ setImportance, group, importance }) {

    return (
        <div className="importance">
            <div className="importance-content">
                <button className={('Primary'===importance.name) ? "selected" : ""} onClick={() => setImportance(group, 'Primary')}>{'Primary'}</button>
                <button className={('Secondary'===importance.name) ? "selected" : ""} onClick={() => setImportance(group, 'Secondary')}>{'Secondary'}</button>
                <button className={('Tertiary'===importance.name) ? "selected" : ""} onClick={() => setImportance(group, 'Tertiary')}>{'Tertiary'}</button>
            </div>
        </div>
    )
}

const mapDispatchToProps = dispatch => ({
    setImportance: (groupName, importance) => dispatch(setImportance(groupName, importance))
});

export default connect(
    null,
    mapDispatchToProps
)(SetImportance);

