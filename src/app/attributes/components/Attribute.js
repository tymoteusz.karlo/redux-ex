import React, { Component } from 'react';
import PropTypes from "prop-types";
import Dot from '../../visuals/Dot';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { setValueAtt } from '../../actions/attributes';


const Dots = styled.div`
    display: flex;
`
class Attribute extends Component {

    static propTypes = {
        value: PropTypes.number
    };

    handleSetValue = (attributeValue) => {
        const { id, setValueAtt, group, importance } = this.props
        setValueAtt(id, attributeValue, group, importance)
    }

    render() {
        const buttons = [1, 2, 3, 4, 5]
        return (
            <div key={this.props.name} className="attribute-container">
                <div className={this.props.name}>{this.props.name}</div>
                <div className={this.props.value} key={this.props.value}>{this.props.value}</div>
                <Dots>
                    {buttons.map(item => {
                        return <Dot
                            key={item} value={item} setValueAtt={this.handleSetValue} pressed={this.props.value < item ? false : true} onClick={(e) => this.handleChange(e)} type={'attribute'} />
                    })}
                </Dots>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setValueAtt: (id, value, group, importance) => {
            dispatch(setValueAtt(id, value, group, importance))
        }
    }
}

export default connect(null, mapDispatchToProps)(Attribute)