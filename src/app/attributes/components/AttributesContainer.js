
import { connect } from 'react-redux'; import React, { Component } from 'react';
import AttributesGroup from './AttributesGroup';

export class AttributesContainer extends Component {

    render() {
        return (
            <div className="attributes-section">
                <div>ATTRIBUTES</div>
                <div className="attributes-container">
                    <AttributesGroup attributes={this.props.attributes.Physical} group={'Physical'} pointsSpent={this.props.attributes.pointsSpent[0]} pointsToSpend={this.props.attributes.pointsToSpend} importance={this.props.attributes.importance[0]} />
                    <AttributesGroup attributes={this.props.attributes.Social} group={'Social'} pointsSpent={this.props.attributes.pointsSpent[1]} pointsToSpend={this.props.attributes.pointsToSpend} importance={this.props.attributes.importance[1]} />
                    <AttributesGroup attributes={this.props.attributes.Mental} group={'Mental'} pointsSpent={this.props.attributes.pointsSpent[2]} pointsToSpend={this.props.attributes.pointsToSpend} importance={this.props.attributes.importance[2]} />
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        attributes: state.attributes
    }
}

export default connect(mapStateToProps, null)(AttributesContainer)