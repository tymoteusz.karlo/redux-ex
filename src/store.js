import { createStore, applyMiddleware }from 'redux';
import rootReducer from './reducers'
import { composeWithDevTools } from 'redux-devtools-extension';
import thunkMiddleware from 'redux-thunk'

const middleWare = [thunkMiddleware]

const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(...middleWare)))
window.store = store

export default store