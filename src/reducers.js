import { combineReducers } from 'redux'
import abilities from './app/reducers/abilities'
import attributes from './app/reducers/attributes'
import characterInfo from './app/reducers/characterInfo'

const rootReducer = combineReducers({
    characterInfo,
    attributes,
    abilities
})

export default rootReducer